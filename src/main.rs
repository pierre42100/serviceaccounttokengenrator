use gouth::Builder;

fn main() {

    let credentials = include_str!("../service-account.json");

    let token = Builder::new()
        .scopes(&["https://www.googleapis.com/auth/firebase.messaging"])
        .json(credentials)
        .build()
        .unwrap();
    println!("authorization: {}", token.header_value().unwrap());
}
